FROM golang:1.21 as builder
WORKDIR /app
COPY . .
RUN go build
RUN apt update && apt install xz-utils jq -y
RUN curl -s https://api.github.com/repos/upx/upx/releases/latest | jq '.assets[] | select(.name?|match("upx-.*-amd64_linux.tar.xz"))' | jq .browser_download_url| tr -d \" | wget -i -
RUN tar -xf upx* && \
 upx*/upx -9 -f tcp-over-websocket
FROM ubuntu
WORKDIR /app
COPY --from=builder /app/tcp-over-websocket .
RUN apt update -qq && apt install -qq ca-certificates -y > /dev/null && apt-get clean && rm -rf /var/lib/apt/lists/*  
# RUN chmod +x /app/tcp-over-websocket
ENTRYPOINT [ "/app/tcp-over-websocket" ]